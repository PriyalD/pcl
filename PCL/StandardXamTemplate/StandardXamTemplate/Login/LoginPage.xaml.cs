﻿using Rg.Plugins.Popup.Extensions;
using StandardXamTemplate.Resx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace StandardXamTemplate
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        PickerPopUp _LangPicker;
        string langValue = "en-US";
        public LoginPage()
        {
            InitializeComponent();
            this.Title = AppResources.Login;

            GetLangList();
           
            TimeZoneLabel.Text = "English";
            TimeZoneLabel.TextColor = Color.Black;
            langValue = "en-US";

            var TimeTapGestureRecognizer = new TapGestureRecognizer();
            TimeTapGestureRecognizer.Tapped += async (s, e) =>
            {
                await Navigation.PushPopupAsync(_LangPicker, false);
            };
            TimeZoneLabel.GestureRecognizers.Add(TimeTapGestureRecognizer);

            SetLanguage();
            SetLabels();

            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            //DisplayAlert("Alert", "Test", "ok");
        }
        private void GetLangList()
        {
            _LangPicker = new PickerPopUp();
            _LangPicker.FilterEvent += new PickerPopUp.FilterDelegate(SetLang);
        }

        private void SetLang(string key, string value)
        {
            TimeZoneLabel.Text = key;
            TimeZoneLabel.TextColor = Color.Black;
            langValue = value;
            SetLanguage();
            SetLabels();
        }
        private void SetLanguage()
        {
            var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo(langValue);

            Resx.AppResources.Culture = ci; // set the RESX for resource localization
            Application.Current.Properties["cultureinfo"] = langValue;
            
            DependencyService.Get<ILocalize>().SetLocale(ci);
        }

        private void SetLabels()
        {
            txtUserName.Placeholder = AppResources.UserName;
            txtPassword.Placeholder = AppResources.Password;
            lblWeDoWelcome.Text = AppResources.Welcome;
            lblSignUp.Text = AppResources.SignUp;
            btnSignIn.Text = AppResources.SignIn;
            lblForgetPassword.Text = AppResources.ForgetPassword;
        }

        protected void btnSignIn_Clicked(object sender, EventArgs e)
        {
            //click event
        }
    }
}