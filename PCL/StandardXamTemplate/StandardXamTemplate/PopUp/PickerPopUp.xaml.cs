﻿using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace StandardXamTemplate
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PickerPopUp : PopupPage
    {
        public delegate void FilterDelegate(string message, string value);
        public event FilterDelegate FilterEvent;
        public List<FilterView> lstFilter = new List<FilterView>();
        string callingPage="Login";
        public PickerPopUp()
        {
            InitializeComponent();
            //this.callingPage = callingPage;
            BindStatusPicker();
        }

        private void BindStatusPicker()
        {
            try
            {
                //Bind your list here
                //from api, static or from AppResources
                //change the api based on callingPage string

                //lstFilter.Add(new FilterView { key = AppResources.Close, value = "Closed" });
               // if (callingPage == "Login")
                {
                    lstFilter.Add(new FilterView { key = "English", value = "en-US" });
                    lstFilter.Add(new FilterView { key = "Japanese", value = "ja-JP" });
                }
                //else
                //{
                //    lstFilter.Add(new FilterView { key = "All", value = "All" });
                //    lstFilter.Add(new FilterView { key = "Open", value = "Open" });
                //    lstFilter.Add(new FilterView { key = "Closed", value = "Closed" });
                //}
                lstStatus.ItemsSource = lstFilter;
            }
            catch
            { //handle exception
            }
        }
        private async void btnClose_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopPopupAsync();
        }
        public void RaiseEvent(string message, string value)
        {
            if (FilterEvent != null)
                FilterEvent(message, value);
        }

        private void CountryListView_ItemSelected(object sender, ItemTappedEventArgs e)
        {
            FilterView obj;

            obj = (FilterView)e.Item;

            RaiseEvent(obj.key, obj.value);

            PopupNavigation.PopAsync();
        }

        private void OnClose(object sender, EventArgs e)
        {
            PopupNavigation.PopAsync();
        }

        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(0.5);
        }


        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(1);
        }
    }

    public class FilterView
    {
        public string key { get; set; }
        public string value { get; set; }
    }
}