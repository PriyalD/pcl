﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StandardXamTemplate
{ 
    public static class Constants
    {
        public static string ServiceURL
        {
            get
            {
                if (AppMode == "UAT")
                {
                    return uatServiceURL;
                }
                else if (AppMode == "PROD")
                {
                    return prodServiceURL;
                }
                else if (AppMode == "DEV")
                {
                    return devServiceURL;
                }
                else
                {
                    return devServiceURL;
                }

            }
        }
        private const string devServiceURL = "http://fjwedomachine1.westindia.cloudapp.azure.com/WeDoTest/api/"; 
        private const string uatServiceURL = "http://10.34.38.115/WeDoWebAPIPublish/api/";
        private const string prodServiceURL = "http://10.34.38.115/WeDoWebAPIPublish/api/";

        private const string AppMode = "DEV";

        public const string AutorizationWord = "Authorization";
        public const string BearerWord = "Bearer ";
        public const string AppUrlencoded = "urlencoded";
        public const string AppJSonWord = "application/json";

        public const string ResourceId = "StandardXamTemplate.Resx.AppResources";
    }
}
