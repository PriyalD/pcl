﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace StandardXamTemplate
{
    [Preserve(AllMembers =true)]
    public class ErrorCodeJson
    {
        public ErrorCodeJson()
        {
            errorCode = string.Empty;
            message = string.Empty;
            recordIDS = new List<string>();
        }

        public string errorCode { get; set; }
        public string message { get; set; }

        public IList<string> recordIDS { get; set; }

        public bool HasError()
        {
            if (errorCode != string.Empty || message != string.Empty)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
