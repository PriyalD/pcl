﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace StandardXamTemplate
{
    [Preserve(AllMembers =true)]
    abstract public class EntityModelErrorBase : EntityModelBase
    {
        protected EntityModelErrorBase()
        {
            ErrorsReturn = new ErrorCodeJson[] { new ErrorCodeJson() };
        }

        public ErrorCodeJson[] ErrorsReturn { get; set; }

        public string Message { get; set; }
        public string MessageCode { get; set; }

        public bool HasError()
        {
            if (ErrorsReturn == null)
            {
                return false;
            }
            else
            {
                foreach (ErrorCodeJson errorReturn in ErrorsReturn)
                {
                    if (errorReturn.errorCode != string.Empty || errorReturn.message != string.Empty)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
        }
    }
}
