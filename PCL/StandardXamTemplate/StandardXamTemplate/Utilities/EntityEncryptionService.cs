﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StandardXamTemplate
{
    public static class EntityEncryptionService
    {
        public static string EncryptObject(string plainText,byte[] salt)
        {
            string enStrData = string.Empty;
            var encryptedText = Crypto.EncryptAes(plainText, "MySecretKey", salt);
            enStrData = Convert.ToBase64String(encryptedText);
            return enStrData;
        }

        public static string DecryptObject(string encryptedText, byte[] salt)
        {
            byte[] enStrData = Convert.FromBase64String(encryptedText);
            var plainText = Crypto.DecryptAes(enStrData, "MySecretKey", salt);
            // return plainText;
            string deStrData = Convert.ToBase64String(plainText);
            //return Encoding.UTF8.GetString(bytes, 0, bytes.Length);
            return deStrData;
        }
    }
}
