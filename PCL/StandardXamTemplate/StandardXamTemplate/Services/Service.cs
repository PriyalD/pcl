﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ModernHttpClient;
using StandardXamTemplate;

using Xamarin.Forms;

namespace StandardXamTemplate
{
    public class Service
    {
        #region " Single Instance "
        private static Service instance;
        public static Service Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Service();
                }
                return instance;
            }
        }

        private Service()
        {
        }
        #endregion
        
        #region "Private methods for POST, GET, PATCH"

        private async Task<string> GetData(string locationURL, string queryURL)
        {
            HttpResponseMessage response;

            string result = string.Empty;
            
            string restQuery = Constants.ServiceURL + locationURL + queryURL;
            HttpClient queryClient = null;

            queryClient = new HttpClient();


            queryClient.Timeout = TimeSpan.FromMinutes(5);

            //new Http message
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, restQuery);

            //add token to header
            request.Headers.Add(Constants.AutorizationWord, Constants.BearerWord);//add token to bearer

            //return JSON to the caller
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.AppJSonWord));
            try
            {
                //call endpoint async
                response = await queryClient.SendAsync(request);

                //read string result
                result = await response.Content.ReadAsStringAsync();
            }
            catch
            {
                //handle exception here
            }
            return result;
        }

        /// <summary>
        /// Post Data aSync
        /// </summary>
        /// <param name="locationURL"></param>
        /// <param name="queryURL"></param>
        /// <returns></returns>
        private async Task<string> PostData(string locationURL, string queryURL, EntityModelBase objPost)
        {
            HttpResponseMessage response;

            string result = string.Empty;
            
            string restQuery = Constants.ServiceURL + locationURL + queryURL;

            HttpClient queryClient = null;

            queryClient = new HttpClient();

            queryClient.Timeout = TimeSpan.FromMinutes(5);
            //add token to header
            queryClient.DefaultRequestHeaders.Add(Constants.AutorizationWord, Constants.BearerWord);

            //return JSON to the caller
            queryClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.AppJSonWord));

            // SerializeObject
            string postBody = JsonConvert.SerializeObject(objPost);

            // Body Content JSON Values
            StringContent bodyContent = new StringContent(postBody, Encoding.UTF8, Constants.AppJSonWord);

            try
            {
                //call endpoint async
                response = await queryClient.PostAsync(restQuery, bodyContent);

                //read string result
                result = await response.Content.ReadAsStringAsync();


            }
            catch
            {
                //handle exception here
            }
            return result;
        }

        /// <summary>
        /// Patch Data aSync (in Progress)
        /// </summary>
        /// <param name="locationURL"></param>
        /// <param name="queryURL"></param>
        /// <returns></returns>
        private async Task<string> PatchData(string locationURL, string queryURL, EntityModelBase objPost)
        {
            HttpResponseMessage response;

            string result = string.Empty;

            string restQuery = Constants.ServiceURL + locationURL + queryURL;

            HttpClient queryClient = null;

            queryClient = new HttpClient();

            queryClient.Timeout = TimeSpan.FromMinutes(5);

            //add token to header
            queryClient.DefaultRequestHeaders.Add(Constants.AutorizationWord, Constants.BearerWord);

            //return JSON to the caller
            queryClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.AppJSonWord));

            // SerializeObject
            string postBody = JsonConvert.SerializeObject(objPost);

            // Body Content JSON Values
            StringContent bodyContent = new StringContent(postBody, Encoding.UTF8, Constants.AppJSonWord);

            var method = new HttpMethod("PATCH");

            var request = new HttpRequestMessage(method, restQuery)
            {
                Content = bodyContent
            };

            try
            {

                //call endpoint async
                response = await queryClient.SendAsync(request);

                //read string result
                result = await response.Content.ReadAsStringAsync();
            }
            catch
            {
                //handle exception here
            }
            return result;
        }
        #endregion

        #region "Private Methods"

        /// <summary>
        /// Validate http response by verifying response code
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private bool IsResultValid(string result)
        {
            if (result.Contains("errorCode"))
            {
                return false;
            }
            else
            {
                if (result.Contains(@"returnCode"":400"))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        private ErrorCodeJson[] ReturnErrorJSON(string result)
        {
            ErrorCodeJson[] errorCodeJson;

            try
            {
                errorCodeJson = JsonConvert.DeserializeObject<ErrorCodeJson[]>(result);

                if (!errorCodeJson[0].HasError())
                {
                    errorCodeJson = GetErrorAsIs(result);
                }
            }
            catch
            {
                errorCodeJson = GetErrorAsIs(result);
            }

            return errorCodeJson;
        }

        private ErrorCodeJson[] GetErrorAsIs(string result)
        {
            // To many different erros from API, send it "as is"
            string sErrorCode = "400";
            string sMessage = result;

            ErrorCodeJson[] errorCodeJSON = new ErrorCodeJson[]
            {
                    new ErrorCodeJson() { errorCode = sErrorCode,
                                          message =  sMessage }
            };

            return errorCodeJSON;
        }

        #endregion
    }
}
