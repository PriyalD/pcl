﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using StandardXamTemplate.Droid;
using StandardXamTemplate;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(NativeEntry), typeof(NativeEntryRenderer))]
namespace StandardXamTemplate.Droid
{
    public class NativeEntryRenderer : EntryRenderer
    {
        public NativeEntryRenderer()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Entry> e)
        {
            base.OnElementChanged(e);
            if (this.Control == null) return;

            var entry = this.Control;

            entry.SetBackgroundResource(Resource.Drawable.selector_edittext);
        }
    }
}