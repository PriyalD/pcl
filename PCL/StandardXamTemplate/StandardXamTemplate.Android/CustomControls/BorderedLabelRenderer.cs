﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using StandardXamTemplate;
using Xamarin.Forms;
using StandardXamTemplate.Droid.CustomControls;

[assembly: ExportRenderer(typeof(BorderedLabel), typeof(BorderedLabelRenderer))]
namespace StandardXamTemplate.Droid.CustomControls
{
    public class BorderedLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            try
            {
                base.OnElementChanged(e);
                if (this.Control == null) return;

                var label = this.Control;

                label.SetBackgroundResource(Resource.Drawable.selector_edittext);
                label.SetPadding(10, 30, 5, 30);
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
        }
    }
}