﻿
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using StandardXamTemplate;
using StandardXamTemplate.iOS;
using UIKit;

[assembly: ExportRenderer(typeof(BorderedLabel), typeof(BorderedLabelRenderer))]
namespace StandardXamTemplate.iOS
{
    public class BorderedLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if (Element != null && Control != null)
            {
                SetNativeControl(new PaddedLabel());

                Control.Layer.CornerRadius = 0;
                Control.Layer.BorderWidth = .5f;
                Control.Layer.BorderColor = UIColor.FromRGB(200, 200, 200).CGColor;
                Control.Layer.MasksToBounds = true;

                Element.HeightRequest = 35;
                base.OnElementChanged(e);
            }
        }
    }

    public sealed class PaddedLabel : UILabel
    {
        private UIEdgeInsets EdgeInsets { get; set; }

        public PaddedLabel()
        {
            EdgeInsets = new UIEdgeInsets(0, 10, 0, 10);
        }

        public override void DrawText(CoreGraphics.CGRect rect)
        {
            base.DrawText(EdgeInsets.InsetRect(rect));
        }
    }
}
