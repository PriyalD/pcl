﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StandardXamTemplate;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using StandardXamTemplate.iOS;
using CoreGraphics;
using Foundation;
using UIKit;

[assembly: ExportRenderer(typeof(NativeEntry), typeof(NativeEntryRenderer))]
namespace StandardXamTemplate.iOS
{
    public class NativeEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Element != null && Control != null)
            {
                // Create a custom border with square corners
                Control.BorderStyle = UITextBorderStyle.None;
                Control.Layer.CornerRadius = 0;
                Control.Layer.BorderWidth = .5f;
                Control.Layer.BorderColor = UIColor.FromRGB(200, 200, 200).CGColor;

                // Invisible views create padding at the beginning and end
                Control.LeftView = new UIView(new CGRect(0, 0, 8, Control.Frame.Height));
                Control.RightView = new UIView(new CGRect(0, 0, 8, Control.Frame.Height));
                Control.LeftViewMode = UITextFieldViewMode.Always;
                Control.RightViewMode = UITextFieldViewMode.Always;

                // Fixed height creates padding at top and bottom
                Element.HeightRequest = 35;
            }
        }
    }
}